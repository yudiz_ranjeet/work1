const express = require('express')
const app = express()

const db = require('./models')
    // const user = require('./controllers/user')

app.get('/', (req, res) => {
        res.send("Home page")
    })
    // app.get('/create', user.create)
    // app.get('/read', user.read)
    // app.get('/update', user.update)
    // app.get('/remove', user.remove)

db.sequelize.sync().then((req) => {
    app.listen(8080, function() {
        console.log('example app listening on port 3000')
    })
})